const express = require('express');
const path = require('path');
const opn = require('opn');
const google = require('./modules/google');

const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');

  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE',
  );

  next();
});

// Serve the static files from the React app
app.use(
  express.static(
    path.join(__dirname, '/..', '..', 'build'),
  ),
);

app.get('/google', (req, res) => {
  google(req.query.search, 10, response => {
    res.send(response);
  });
});

app.get('*', (req, res) => {
  res.sendFile(
    path.join(__dirname, '/..', '..', 'build/index.html'),
  );
});

app.listen(8000);

opn('http://localhost:8000');
