// var request = require('request')
const axios = require('axios');
const cheerio = require('cheerio');
const querystring = require('querystring-browser');
const util = require('util');

const linkSel = 'h3.r a';
const descSel = 'div.s';
const itemSel = 'div.g';
const nextSel = 'td.b a span';

let URL =
  '%s://www.google.%s/search?hl=%s&q=%s&start=%s&sa=N&num=%s&ie=UTF-8&oe=UTF-8&gws_rd=ssl';

const nextTextErrorMsg =
  'Translate `google.nextText` option to selected language to detect next results link.';
const protocolErrorMsg =
  "Protocol `google.protocol` needs to be set to either 'http' or 'https', please use a valid protocol. Setting the protocol to 'https'.";

// start parameter is optional
const google = (query, start, callback) => {
  let startIndex = 0;
  let next = callback;
  if (typeof callback === 'undefined') {
    next = start;
  } else {
    startIndex = start;
  }
  igoogle(query, startIndex, next);
};

google.resultsPerPage = 50;
google.tld = 'com';
google.lang = 'en';
google.requestOptions = {};
google.nextText = 'Next';
google.protocol = 'https';

const igoogle = async (query, start, callback) => {
  if (google.resultsPerPage > 100)
    google.resultsPerPage = 100; // Google won't allow greater than 100 anyway
  if (google.lang !== 'en' && google.nextText === 'Next')
    console.warn(nextTextErrorMsg);
  if (
    google.protocol !== 'http' &&
    google.protocol !== 'https'
  ) {
    google.protocol = 'https';
    console.warn(protocolErrorMsg);
  }

  // timeframe is optional. splice in if set
  if (google.timeSpan) {
    URL =
      URL.indexOf('tbs=qdr:') >= 0
        ? URL.replace(
            /tbs=qdr:[snhdwmy]\d*/,
            `tbs=qdr:${google.timeSpan}`,
          )
        : URL.concat('&tbs=qdr:', google.timeSpan);
  }
  const newUrl = util.format(
    URL,
    google.protocol,
    google.tld,
    google.lang,
    querystring.escape(query),
    start,
    google.resultsPerPage,
  );
  const requestOptions = {
    url: newUrl,
    method: 'GET',
  };

  const response = await axios(requestOptions);

  const body = response.data;

  try {
    const $ = cheerio.load(body);
    const res = {
      url: newUrl,
      query,
      start,
      links: [],
      $,
      body,
    };

    $(itemSel).each((i, elem) => {
      const linkElem = $(elem).find(linkSel);
      const descElem = $(elem).find(descSel);
      const item = {
        title: $(linkElem)
          .first()
          .text(),
        link: null,
        description: null,
        href: null,
      };
      const qsObj = querystring.parse(
        $(linkElem).attr('href'),
      );

      if (qsObj['/url?q']) {
        item.link = qsObj['/url?q'];
        item.href = item.link;
      }

      $(descElem)
        .find('div')
        .remove();
      item.description = $(descElem).text();

      res.links.push(item);
    });

    if (
      $(nextSel)
        .last()
        .text() === google.nextText
    ) {
      res.next = () => {
        igoogle(
          query,
          start + google.resultsPerPage,
          callback,
        );
      };
    }

    // const withoutFirstTen = res.links.slice(9);
    // callback(withoutFirstTen);
    callback(res.links);
  } catch (err) {
    callback(err);
  }
};

module.exports = google;
