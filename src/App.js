import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import Routes from './Routes';
import rootReducer from './reducers';
import middleware from './middleware';

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middleware)),
);

const App = () => (
  <Provider store={store}>
    <Router>
      <MuiThemeProvider>
        <Routes />
      </MuiThemeProvider>
    </Router>
  </Provider>
);

export default App;
