// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import { CSVLink } from 'react-csv';
import RaisedButton from 'material-ui/RaisedButton';
import Row from './ui/Row';

export type Props = {
  emails: Array,
  searchTerm: string,
};

const Download = ({ emails, searchTerm }: Props) =>
  emails &&
  emails.length > 0 && (
    <Row
      style={{
        justifyContent: 'flex-end',
        width: '100vw',
        paddingRight: 30,
        marginBottom: 10,
      }}
    >
      <CSVLink data={emails} filename={`${searchTerm}.csv`}>
        <RaisedButton label="Download CSV" primary />
      </CSVLink>
    </Row>
  );

const mapStateToProps = state => ({
  emails: state.results.emails,
  searchTerm: state.search.value,
});

export default connect(mapStateToProps)(Download);
