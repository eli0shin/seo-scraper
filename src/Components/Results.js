// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress';

type Props = {
  emails: Array<Object>,
  inProgress: boolean,
};

const Results = ({ emails, inProgress }: Props) => {
  if (inProgress) {
    return (
      <CircularProgress
        size={80}
        thickness={10}
        style={{ marginTop: 40 }}
      />
    );
  }

  return typeof emails !== 'undefined' &&
    emails.length > 0 ? (
    <Table>
      <TableHeader
        displaySelectAll={false}
        adjustForCheckbox={false}
      >
        <TableRow>
          <TableHeaderColumn>First Name</TableHeaderColumn>
          <TableHeaderColumn>Last Name</TableHeaderColumn>
          <TableHeaderColumn>Email</TableHeaderColumn>
          <TableHeaderColumn>Domain Name</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {emails.map(contact => (
          <TableRow>
            <TableRowColumn>
              {contact.firstName}
            </TableRowColumn>
            <TableRowColumn>
              {contact.lastName}
            </TableRowColumn>
            <TableRowColumn>{contact.email}</TableRowColumn>
            <TableRowColumn>
              {contact.domain}
            </TableRowColumn>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  ) : null;
};

const mapStateToProps = state => ({
  emails: state.results.emails,
  inProgress: state.results.inProgress,
});

export default connect(mapStateToProps)(Results);
