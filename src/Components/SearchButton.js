// @flow
import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import Search from 'react-icons/lib/md/search';

const SearchButton = () => (
  <FlatButton type="submit" style={{ marginBottom: 8 }}>
    <Search />
  </FlatButton>
);
export default SearchButton;
