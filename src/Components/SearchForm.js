import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import SearchField from '../Containers/SearchField';
import Results from './Results';
import Download from './Download';

const SearchForm = () => (
  <div className={css(styles.container)}>
    <SearchField />
    <Download />
    <Results />
  </div>
);

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '100vh',
  },
});

export default SearchForm;
