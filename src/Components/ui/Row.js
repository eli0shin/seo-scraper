import React from 'react';
import { StyleSheet, css } from 'aphrodite';

const Row = ({ children, style }) => (
  <div className={css(styles.row)} style={style}>
    <React.Fragment>{children}</React.Fragment>
  </div>
);

Row.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
});

export default Row;
