import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField';
import Row from '../Components/ui/Row';
import SearchButton from '../Components/SearchButton';
import {
  updateSearchValue,
  submitSearch,
} from '../actions/search';

class SearchField extends Component {
  submitSearch = e => {
    e.preventDefault();
    this.props.submitSearch();
  };

  render() {
    const {
      searchValue,
      updateSearchValue: updateValue,
    } = this.props;
    return (
      <form onSubmit={this.submitSearch}>
        <Row style={{ alignItems: 'flex-end' }}>
          <React.Fragment>
            <TextField
              value={searchValue}
              onChange={updateValue}
              floatingLabelText="Search Term"
              id="search-field"
            />
            <SearchButton />
          </React.Fragment>
        </Row>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  searchValue: state.search.value,
});

export default connect(mapStateToProps, {
  updateSearchValue,
  submitSearch,
})(SearchField);
