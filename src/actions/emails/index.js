// @flow
import {
  GET_HUNTER_EMAILS,
  HUNTER_EMAILS_RECEIVED,
  CONTACT_FINALIZED,
} from '../';

export const getHunterEmails = (domain: string) => ({
  type: GET_HUNTER_EMAILS,
  payload: {
    domain,
  },
});

export const hunterEmailReceived = (
  domain: string,
  emails: Array<Object>,
) => ({
  type: HUNTER_EMAILS_RECEIVED,
  payload: {
    domain,
    emails,
  },
});

export const contactFinalized = (
  firstName: string,
  lastName: string,
  email: string,
  domain: string,
) => ({
  type: CONTACT_FINALIZED,
  payload: {
    contact: { firstName, lastName, email, domain },
  },
});
