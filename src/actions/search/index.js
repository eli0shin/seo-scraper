// @flow
import {
  SEARCH_TERM_UPDATED,
  SUBMIT_SEARCH,
  LINKS_RECEIVED,
  LINKS_CLEANED,
} from '../';

export const updateSearchValue = (event, newValue) => ({
  type: SEARCH_TERM_UPDATED,
  payload: { value: newValue },
});

export const submitSearch = () => ({
  type: SUBMIT_SEARCH,
});

export const linksReceived = (links: Array<Object>) => ({
  type: LINKS_RECEIVED,
  payload: {
    links,
  },
});

export const linksCleaned = (links: Array<Object>) => ({
  type: LINKS_CLEANED,
  payload: {
    links,
  },
});
