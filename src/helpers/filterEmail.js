// @flow
const domainList = [
  'gmail.com',
  'aol.com',
  'live.com',
  'outlook.com',
  'yahoo.com',
  'me.com',
  'mail.com',
];

const filterEmail = (email: string, domain: string) =>
  email.split('@')[1].toLowerCase() === domain ||
  domainList.includes(email.split('@')[1].toLowerCase());

export default filterEmail;
