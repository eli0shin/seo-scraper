// @flow
import axios from 'axios';
import { GET_HUNTER_EMAILS } from '../../actions';
import { hunterEmailReceived } from '../../actions/emails';
import config from '../../../config';

const BASE_URL = 'https://api.hunter.io/v2/domain-search';
const type = 'personal';
const department = 'executive, management, marketing';
const API_KEY = config.hunter.apiKey;

const getEmailFromURL = ({
  dispatch,
}) => next => async action => {
  next(action);
  if (action.type === GET_HUNTER_EMAILS) {
    try {
      const { domain } = action.payload;

      const res = await axios({
        method: 'GET',
        url: `${BASE_URL}?domain=${domain}&type=${type}&department=${department}&api_key=${API_KEY}`,
      });

      dispatch(hunterEmailReceived(domain, res.data.data));
    } catch (err) {
      console.log(err);
    }
  }
};

export default getEmailFromURL;
