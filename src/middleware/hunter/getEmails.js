// @flow
import { LINKS_CLEANED } from '../../actions';
import { getHunterEmails } from '../../actions/emails';

const getEmails = ({
  dispatch,
}) => next => async action => {
  next(action);
  if (action.type === LINKS_CLEANED) {
    try {
      const { links } = action.payload;

      links.some((link, index) => {
        dispatch(getHunterEmails(link.link));

        // hunter limits us to 15 req/second
        if (index === 14) {
          return true;
        }
        return false;
      });
    } catch (err) {
      console.log(err);
    }
  }
};

export default getEmails;
