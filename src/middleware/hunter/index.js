import getEmails from './getEmails';
import getEmailFromURL from './getEmailFromURL';

export default [getEmails, getEmailFromURL];
