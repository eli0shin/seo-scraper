import search from './search';
import hunter from './hunter';
import whoIs from './whoIs';

export default [...search, ...hunter, ...whoIs];
