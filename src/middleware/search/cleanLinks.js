// @flow
import URL from 'wurl';
import { LINKS_RECEIVED } from '../../actions';
import { linksCleaned } from '../../actions/search';

const cleanLinks = ({
  dispatch,
}) => next => async action => {
  next(action);
  if (action.type === LINKS_RECEIVED) {
    try {
      const { links } = action.payload;

      const cleanedLinks = links.filter(link => {
        if (link.href && link.href !== null) {
          const path = URL('path', link.href);
          if (path.length < 2) {
            return URL('domain', link.href);
          }
        }
        return false;
      });

      dispatch(linksCleaned(cleanedLinks));
    } catch (err) {
      console.log(err);
    }
  }
};

export default cleanLinks;
