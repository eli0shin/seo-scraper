import submitSearch from './submitSearch';
import cleanLinks from './cleanLinks';

export default [submitSearch, cleanLinks];
