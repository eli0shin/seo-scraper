// @flow

import axios from 'axios';
import { SUBMIT_SEARCH } from '../../actions';
import { linksReceived } from '../../actions/search';

const submitSearch = ({
  dispatch,
  getState,
}) => next => async action => {
  next(action);

  if (action.type === SUBMIT_SEARCH) {
    const store = getState();
    const { value } = store.search;

    try {
      const res = await axios({
        method: 'GET',
        url: `http://localhost:8000/google?search=${encodeURIComponent(
          value,
        )}`,
      });

      dispatch(linksReceived(res.data));
    } catch (err) {
      console.log(err);
    }
  }
};

export default submitSearch;
