// @flow
import axios from 'axios';
import type { Store, next as nextType } from 'redux';
import { HUNTER_EMAILS_RECEIVED } from '../../actions';
import filterEmail from '../../helpers/filterEmail';
import { contactFinalized } from '../../actions/emails';
import config from '../../../config';

const BASE_URL =
  'https://www.whoisxmlapi.com/whoisserver/WhoisService?';
const API_KEY = config.whoIs.apiKey;

const getWhoIs = ({ dispatch }: Store) => (
  next: nextType,
) => async (action: Object) => {
  next(action);
  if (action.type === HUNTER_EMAILS_RECEIVED) {
    const { domain } = action.payload.emails;

    if (
      action.payload.emails.emails &&
      action.payload.emails.emails.length > 0
    ) {
      action.payload.emails.emails.forEach(
        ({ first_name, last_name, value }) =>
          dispatch(
            contactFinalized(
              first_name,
              last_name,
              value,
              domain,
            ),
          ),
      );
    } else {
      try {
        const res = await axios({
          method: 'GET',
          url: `${BASE_URL}domainName=${domain}&apiKey=${API_KEY}&outputFormat=json`,
        });

        const {
          data: {
            WhoisRecord: { administrativeContact },
          },
        } = res;

        console.log(administrativeContact);

        if (typeof administrativeContact !== 'undefined') {
          const { email, name } = administrativeContact;

          const firstName =
            typeof name !== 'undefined'
              ? name.split(', ')[1]
              : '';

          const lastName =
            typeof name !== 'undefined'
              ? name.split(', ')[0]
              : '';

          if (
            typeof email !== 'undefined' &&
            filterEmail(email, domain)
          ) {
            dispatch(
              contactFinalized(
                firstName,
                lastName,
                email,
                domain,
              ),
            );
          }
        }
      } catch (err) {
        console.log(err);
      }
    }
  }
};

export default getWhoIs;
