import {
  SUBMIT_SEARCH,
  CONTACT_FINALIZED,
} from '../../actions';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case CONTACT_FINALIZED:
      return [...state, action.payload.contact];
    case SUBMIT_SEARCH:
      return initialState;
    default:
      return state;
  }
};
