import {
  SUBMIT_SEARCH,
  CONTACT_FINALIZED,
} from '../../actions';

const initialState = false;

export default (state = initialState, action) => {
  switch (action.type) {
    case CONTACT_FINALIZED:
      return initialState;
    case SUBMIT_SEARCH:
      return true;
    default:
      return state;
  }
};
