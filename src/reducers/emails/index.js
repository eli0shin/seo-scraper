import { combineReducers } from 'redux';
import emailsReducer from './emailsReceived';
import inProgress from './inProgress';

export default combineReducers({
  emails: emailsReducer,
  inProgress,
});
