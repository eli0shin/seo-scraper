import { combineReducers } from 'redux';
import searchReducer from './search';
import emailsReducer from './emails';

export default combineReducers({
  search: searchReducer,
  results: emailsReducer,
});
