import { SEARCH_TERM_UPDATED } from '../../actions';

const initialState = '';

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_TERM_UPDATED:
      return action.payload.value;
    default:
      return state;
  }
};
